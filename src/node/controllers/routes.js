import axios from "axios";
import bodyParser from 'body-parser';
import multer from 'multer';
import {body, validationResult} from 'express-validator/check';
import {apiKey, apiUrl} from "../const";
import {
    createMovie,
    deleteImage,
    deleteMovie, deleteMoviesOnServerLoading,
    insertImage, insertMoviesOnServerLoading,
    showAllMovies,
    showMovie,
    updateMovie
} from "./movieController";

deleteMoviesOnServerLoading();
insertMoviesOnServerLoading();

const express = require('express');
const app = express();

var upload = multer({dest: '/tmp/'});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

app.get('/api/movies/all', async (req, res) => {
    await showAllMovies().then((movies) => {
        res.send(movies);
    });
});

app.get('/api/movies/:id', async (req, res) => {
    await showMovie(req.params.id).then((movies) => {
        res.send(movies);
    });
});

app.get('/api/movies/picture/:title', (req, res) => {
    let title = req.params.title.toLowerCase().replace(' ', '+');
    axios.get(apiUrl + '?t=' + req.params.title + '&apikey=' + apiKey)
        .then(response => {
            if (response.data.Response === "True") {
                res.send(response.data.Poster);
            } else {
                res.send("Not Found")
            }
        })
        .catch(error => {
            res.send(error);
        })
});

app.post('/api/movies/:id',
    body('title')
        .isString().withMessage("title must be a string")
        .exists().withMessage("title is required"),
    body('year')
        .isNumeric().withMessage("year must be a numeric value")
        .isLength({min: 4, max: 4}).withMessage("year must be in #### format"),
    body('gender')
        .isString().withMessage("gender must be a string")
        .exists().withMessage("gender is required"),
    body('synopsys')
        .isString().withMessage("synopsys must be a string")
        .exists().withMessage("synopsis is required"),
    body('director')
        .exists().withMessage("director is required"),
    body('rating')
        .exists().withMessage("rating is required")
        .isNumeric().withMessage("rating must be a number"),
    upload.any(),
    async (req, res) => {
        var errors = validationResult(req.body);

        if (!errors.isEmpty()) {
            res.status(400).json(errors.array()[0].msg);
        } else {
            await updateMovie(req.params.id, req.body)
                .then(async movie => {
                    if (req.files[0] !== undefined) {
                        await insertImage(movie._id, req.files[0])
                            .then(async () => {
                                await showAllMovies().then((movies) => {
                                    res.send(movies);
                                });
                            })
                            .catch(err => {
                                res.status(500).json("insertion: " + err)
                            })
                    } else {
                        await showAllMovies().then((movies) => {
                            res.send(movies);
                        });
                    }
                })
                .catch(err => {
                    res.status(500).json("update:" + err)
                });
        }
    });

app.post('/api/movies/:id/image', upload.single('image'), async (req, res) => {
    await insertImage(req.params.id, req.file)
        .then(async () => {
            await showAllMovies().then((movies) => {
                res.send(movies);
            });
        })
        .catch(err => {
            res.send(err)
        });
});

app.post('/api/movies',
    body('title')
        .isString().withMessage("title must be a string")
        .exists().withMessage("title is required"),
    body('year')
        .isNumeric().withMessage("year must be a numeric value")
        .isLength({min: 4, max: 4}).withMessage("year must be in #### format"),
    body('gender')
        .isString().withMessage("gender must be a string")
        .exists().withMessage("gender is required"),
    body('synopsys')
        .isString().withMessage("synopsys must be a string")
        .exists().withMessage("synopsis is required"),
    body('director')
        .exists().withMessage("director is required"),
    body('rating')
        .exists().withMessage("rating is required")
        .isNumeric().withMessage("rating must be a number")
    , upload.any()
    , async (req, res) => {
        var errors = validationResult(req.body);

        if (!errors.isEmpty()) {
            res.status(400).json(errors.array()[0].msg);
        } else {
            await createMovie(req.body)
                .then(async movie => {
                    if (req.files[0] !== undefined) {
                        await insertImage(movie._id, req.files[0])
                            .then(() => {
                                res.send(movie)
                            })
                            .catch(err => {
                                res.status(500).json("insertion: " + err)
                            })
                    } else res.send(movie)
                })
                .catch(err => {
                    res.status(500).json("creation:" + err)
                });
        }
    });

app.delete('/api/movies/:id', async (req, res) => {
    await deleteMovie(req.params.id)
        .then(async () => {
            await deleteImage(req.params.id)
                .then(async () => {
                    await showAllMovies().then((movies) => {
                        res.send(movies);
                    });
                });
        });
});

module.exports = app;
