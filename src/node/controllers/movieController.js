import mongoose from "mongoose";
import 'babel-polyfill';
import fs from "fs";
import {listMoviesBase} from '../const';

mongoose.connect('mongodb://localhost/movies');

const Schema = mongoose.Schema;

const MovieSchema = new Schema({
    title: String,
    year: String,
    gender: String,
    synopsys: String,
    rating: Number,
    imageAuto: String,
    director: {
        name: String,
        birthday: String,
        country: String
    }
});
const Movie = mongoose.model('Movie', MovieSchema);

export var deleteMoviesOnServerLoading = function () {
    Movie.remove({}, function (err) {
        console.log('collection droped')
    })
};

export var insertMoviesOnServerLoading = function () {
    listMoviesBase.forEach(async movie => {
        await createMovie(movie)
    })
};

export var showAllMovies = async function () {
    return Movie.find({}).then((m) => {
        return m
    });
};

export var showMovie = async function (id) {
    return Movie.findOne({_id: id}).then((m) => {
        return m
    });
};

export var insertImage = async function (id, file) {
    return new Promise(async (resolve, reject) => {
        var oldpath = file.path;
        var newpath = __dirname + '/../../static/' + id + '.png';
        fs.rename(oldpath, newpath, function (err, data) {
            if (err) reject("rename" + err);
            else resolve();
        })
    })
};

export var deleteMovie = async function (id) {
    return new Promise((resolve, reject) => {
        Movie.findById(id)
            .then(m => {
                m.remove()
                    .then(m => {
                        m.save()
                            .then(() => {
                                resolve()
                            })
                            .catch(e => {
                                reject(e)
                            });
                    })
                    .catch(e => {
                        reject(e)
                    });
            })
            .catch(() => {
                reject("movie not find")
            });
    })
};

export var deleteImage = async function (id) {
    return new Promise((resolve, reject) => {
        let path = __dirname + '/../../static/' + id + '.png';
        fs.stat(path, function (err, stats) {
            if (!err && stats.dev) {
                fs.unlink(path, function (err) {
                    if (err) reject(err);
                    else resolve();
                })
            } else {
                resolve()
            }
        });
    })
};

export var createMovie = async function (movie) {
    const newMovie = new Movie();
    newMovie.title = movie.title;
    newMovie.year = movie.year;
    newMovie.gender = movie.gender;
    newMovie.synopsys = movie.synopsys;
    newMovie.director.name = movie.director_name;
    newMovie.director.birthday = movie.director_birthday;
    newMovie.director.country = movie.director_country;
    newMovie.rating = movie.rating;
    newMovie.imageAuto = movie.imageAuto;

    return newMovie.save()
        .then(movie => {
            return movie;
        })
};

export var updateMovie = async function (id, movie) {
    return new Promise((resolve, reject) => {
        Movie.findByIdAndUpdate(id, {
            title: movie.title,
            year: movie.year,
            gender: movie.gender,
            synopsys: movie.synopsys,
            director: {
                name: movie.director_name,
                birthday: movie.director_birthday,
                country: movie.director_country
            },
            rating: movie.rating,
            imageAuto: movie.imageAuto
        })
            .then(movie => {
                resolve(movie)
            })
            .catch(e => {
                reject("a l'interieur update: " + e)
            });
    });
};
