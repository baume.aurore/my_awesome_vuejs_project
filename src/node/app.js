require('babel-register')({
    presets: [ 'env' ]
});

const express = require('express');
const app = express();

app.use(require('./middlewares/cors'));
app.use(require('./controllers'));

app.listen(3000, () => console.log('Example app listening on port 3000!'));

module.exports = app;
