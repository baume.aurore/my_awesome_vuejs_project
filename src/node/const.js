export const apiKey = "bdf5e086";
export const apiUrl = "http://www.omdbapi.com/";

export const listMoviesBase =
    [
        {
            "title": "L'étrange noël de M. Jack",
            "year": 1994,
            "gender": "Cinéma de fantasy/Musical",
            "synopsys": "Jack Skellington, un épouvantail squelettique surnommé « le Roi des citrouilles » (Pumpkin King en version originale), vit dans la ville d'Halloween. En tant que maître de l'épouvante, Jack occupe ses journées à préparer la prochaine fête d'Halloween.",
            "rating": 3,
            "imageAuto": "https://static.fnac-static.com/multimedia/Images/FR/NR/75/98/9c/10262645/1507-1/tsp20181010115535/L-Etrange-Noel-de-Monsieur-Jack.jpg",
            "director_name": "Henry Selick",
            "director_birthday": "30-11-1952",
            "director_country": "Américain",
        },
        {
            "title": "Interstellar",
            "year": 2014,
            "gender": "Drame / Film à énigme",
            "rating": 4,
            "synopsys": "Alors que la Terre se meurt, une équipe d'astronautes franchit un trou de ver apparu près de Saturne conduisant à une autre galaxie, cela dans le but d'explorer un nouveau système stellaire et l'espoir de trouver une nouvelle planète habitable par l'humanité afin de la sauver.",
            "imageAuto": "https://m.media-amazon.com/images/M/MV5BZjdkOTU3MDktN2IxOS00OGEyLWFmMjktY2FiMmZkNWIyODZiXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SX300.jpg",
            "director_name": "Christopher Nolan",
            "director_birthday": "30-07-1970",
            "director_country": "Américain",
        }
    ];
