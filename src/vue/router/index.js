import Vue from 'vue'
import Router from 'vue-router'
import movieManagement from '../components/movieManagement'
import movieCreate from '../components/movieCreate'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'list',
            component: movieManagement
        },
        {
            path: '/edit/:id',
            name: 'edit',
            component: movieCreate,
            props: {type: 'edit'}
        },
        {
            path: '/add',
            name: 'add',
            component: movieCreate,
            props: {type: 'create'}
        }
    ]
})
