import Vue from 'vue'
import Vuetify from 'vuetify'
import axios from 'axios'
import App from './app.vue'
import router from './router'
import store from './store'

import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'
import MovieItemComponent from './components/movieItem.vue'

window.axios = axios;
window.axios.defaults.baseURL = window.API_SERVER_URL;

Vue.use(Vuetify, {
    theme: {
        primary: colors.deepOrange.accent3,
        secondary: colors.lightGreen.accent3,
        check: colors.lightGreen.accent4,
        error: colors.red.darken1
    },
    options: {
        customProperties: true
    }
});

Vue.component('movie-item', MovieItemComponent);

new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template: '<App/>'
});
