import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex);

const state = {
    movies: [],
    movie: {
        infos: {
            _id: "",
            title: "",
            year: "",
            synopsys: "",
            gender: "",
            rating: 1,
            director: {
                name: "",
                country: "",
                birthday: ""
            }
        },
        image: ""
    },
    apiImage: "",
    errors: {
        type_message: "",
        type_alert: "", // OK, KO, warning
        message: ""
    }
};

// getters
const getters = {
    getMovies(state) {
        return state.movies
    },
    getMoviesSearch(state) {
        return keyword => state.movies.filter(
            m =>
                m.title.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
                || m.synopsys.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
                || m.gender.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
                || m.director.name.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
                || m.director.country.toLowerCase().indexOf(keyword.toLowerCase()) !== -1
        );
    },
    getMovie(state) {
        return state.movie
    },
    getErrors(state) {
        return state.errors
    },
    getApiImage(state) {
        return state.apiImage
    }
};

// mutations
const mutations = {
    setMovies(state, val) {
        state.movies = val
    },
    setMovieParam(state, {prop, val, prop2, info, clear}) {
        if (clear) {
            state.movie = {
                infos: {
                    title: "",
                    year: "",
                    synopsys: "",
                    gender: "",
                    rating: 1,
                    director: {
                        name: "",
                        country: "",
                        birthday: ""
                    }
                },
                image: ""
            }
        } else {
            if (info) {
                if (prop2) {
                    state.movie.infos[prop][prop2] = val
                } else if (prop){
                    state.movie.infos[prop] = val
                } else {
                    state.movie.infos = val
                }
            } else {
                state.movie.image = val
            }
        }
    },
    setErrors(state, val) {
        if (val !== "clear") {
            state.errors = val
        } else {
            state.errors = {
                type_message: "",
                type_alert: "", // OK, KO, warning
                message: ""
            }
        }
    },
    setApiImage(state, val) {
        state.apiImage = val
    }
};

// actions
const actions = {
    getMoviesFromAPI({commit}) {
        return new Promise((resolve, reject) => {
            window.axios
                .get('/api/movies/all')
                .then(
                    response => {
                        commit('setMovies', response.data);
                        resolve()
                    }
                )
                .catch(e => {
                    reject(e)
                })
        })
    },
    getMovieFromAPI({commit}, id) {
        return new Promise((resolve, reject) => {
            window.axios
                .get('/api/movies/' + id)
                .then(
                    response => {
                        commit('setMovieParam', {val: response.data, info: true});
                        if (response.data.imageAuto !== undefined && response.data.imageAuto !== "") {
                            commit('setApiImage', response.data.imageAuto);
                        }
                        resolve()
                    }
                )
                .catch(e => {
                    reject(e)
                })
        })
    },
    setMovieToAPI({state, commit}, type) {
        return new Promise((resolve, reject) => {
            let formData = new FormData();
            if (state.apiImage !== "") {
                formData.append('imageAuto', state.apiImage)
            } else {
                formData.append('image', state.movie.image)
            }
            for (let key in state.movie.infos) {
                if (key !== 'director') {
                    formData.append(key, state.movie.infos[key]);
                } else {
                    for (let k in state.movie.infos[key]) {
                        formData.append(key + "_" + k, state.movie.infos[key][k])
                    }
                }
            }

            let url = (type === 'create' ? '/api/movies' : '/api/movies/'
                + state.movie.infos._id);
            window.axios
                .post(url,
                    formData,
                    {
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        }
                    })
                .then(
                    () => {
                        commit('setMovieParam', {clear: true});
                        commit('setApiImage', "");
                        commit('setErrors', {
                            type_message: "create",
                            type_alert: "OK",
                            message: "Votre film a bien été édité"
                        });
                        resolve()
                    }
                )
                .catch(e => {
                    commit('setApiImage', "");
                    commit('setErrors', {
                        type_message: "create",
                        type_alert: "KO",
                        message: e
                    });
                    reject(e)
                })
        })
    },
    getImageFromApi({commit}, title) {
        return new Promise((resolve, reject) => {
            commit('setErrors', "clear");
            window.axios
                .get('/api/movies/picture/' + title)
                .then(
                    response => {
                        if (response.data !== "Not Found" && response.data !== "N/A") {
                            commit('setApiImage', response.data);
                            commit('setErrors', {
                                type_message: "image_api",
                                type_alert: "OK",
                                message: "Image touvée"
                            });
                        } else {
                            commit('setErrors', {
                                type_message: "image_api",
                                type_alert: "KO",
                                message: "L'image que vous recherchez n'a pas été trouvée, Essayez avec un autre film ou importez votre propre image."
                            });
                        }
                        resolve()
                    }
                )
                .catch(
                    () => {
                        commit('setErrors', {
                            type_message: "image_api",
                            type_alert: "KO",
                            message: "L'image que vous recherchez n'a pas été trouvée, Essayez avec un autre film ou importez votre propre image."
                        });
                        reject()
                    }
                )
        })
    },
    deleteMovieFromAPI({commit}, id) {
        return new Promise((resolve, reject) => {
            window.axios
                .delete('/api/movies/' + id)
                .then(
                    res => {
                        commit('setMovies', res.data);
                        commit('setErrors', {
                            type_message: "delete",
                            type_alert: "OK",
                            message: "Film supprimé"
                        });
                        resolve()
                    }
                )
                .catch(
                    e => {
                        commit('setErrors', {
                            type_message: "delete",
                            type_alert: "KO",
                            message: "Une erreur s'est produite dans la suppression du film"
                        });
                        reject(e)
                    }
                )
        })
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
