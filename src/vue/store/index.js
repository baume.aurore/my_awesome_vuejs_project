import Vue from 'vue'
import Vuex from 'vuex'

import movieManagement from './modules/movie-management'

Vue.use(Vuex)

export default new Vuex.Store({
    modules: {
        movieManagement
    },
    strict: process.env.NODE_ENV !== 'production'
})
